# mysql

# 启动Server
mysqld --defaults-file=/opt/mysql/conf/my.cnf

# 修改root账户密码
mysqladmin -uroot -p'old_passwd' password 'new_passwd'

# Client连接，并修改root账户允许远程连接
mysql -h127.0.0.1 -uroot -p
> use mysql;
> update user set host = '%' where user = 'root';
> flush privileges;


